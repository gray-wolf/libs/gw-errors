use std::path::PathBuf;

use thiserror::Error;

/// User errors, resulting for faulty, missing or incomplete user input.
#[derive(Debug, Error)]
pub enum UserError {
    #[error("invalid command '{0}' provided")]
    InvalidCommand(String),

    #[error("invalid script file '{0}' provided, error text '{1}'")]
    InvalidScriptFile(PathBuf, String),
}
